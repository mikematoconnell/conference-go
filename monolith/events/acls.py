from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    header_dict = {"Authorization": PEXELS_API_KEY}
    parameters = {"query": str(city) + " " + str(state), "per_page": 1} 
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=parameters, headers=header_dict)
    content = json.loads(response.content)
    # get the value of the photo url
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
    

def get_weather(city, state):
    params = {
        "q": f"{city}, {state}, US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    # use request with the paramsa nd url to return the response
    response = requests.get(url, params=params)
    content = json.loads(response)
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except(KeyError, IndexError):
        return None
    
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "http://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    
    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except(KeyError, IndexError):      
        return None


# create the dictionary that will be used in the get call

# add the authorization to the dictionary

# add the pexel query parameter with the location as its value